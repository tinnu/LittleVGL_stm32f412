/*
 * littlevgl_support.h
 *
 *  Created on: Aug 15, 2020
 *      Author: AN
 */

#ifndef LITTLEVGL_SUPPORT_H_
#define LITTLEVGL_SUPPORT_H_

#include <stdint.h>

#define LCD_WIDTH 240
#define LCD_HEIGHT 240
#define LCD_FB_BYTE_PER_PIXEL 1


void lv_port_disp_init(void);
void lv_port_indev_init(void);

#endif /* LITTLEVGL_SUPPORT_H_ */
