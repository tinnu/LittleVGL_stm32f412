# LittleVGL_stm32f412

#### 介绍
LittleVGL-V7.2移植到stm32f412-discovery官方开发板。
驱动芯片ft6x06+st7789h2，屏幕分辨率240x240，MCU屏接口引脚D0-D15，电容触摸屏

#### 版本
200817


#### 版本说明
->200817
删减注释，增加说明


#### 使用说明

使用cubemxIDE开发
PC端仿真LittelVGL工程参考：https://gitee.com/tinnu/EXC_Sreen_LittelVGL_Simualtion


#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


