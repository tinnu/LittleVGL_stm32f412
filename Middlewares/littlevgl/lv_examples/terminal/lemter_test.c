/*
 * dashbox.c
 *
 *  Created on: 2020年7月25日
 *      Author: AN
 */

#include <lv_examples/terminal/lemter_test.h>
#include "lvgl.h"
#include "stdio.h"

lv_style_t main_style;
lv_obj_t *lmeter;
static lv_obj_t * label1;
uint16_t lmeter_value = 0;

void dash_cb(lv_task_t *task)
{
	char buff[10];
	lmeter_value += 5;
	if(lmeter_value>100)
		lmeter_value = 0;
	lv_lmeter_set_value(lmeter,lmeter_value);
	sprintf(buff,"%d%%",lmeter_value);
	lv_label_set_text(label1,buff);
}

void lv_lemter_add()
{


//	lv_obj_t *scr=lv_scr_act();
//
//	lv_style_copy(&main_style, &lv_style_plain_color);
//	main_style.body.main_color = LV_COLOR_GREEN;//活跃刻度线的起始颜色
//	main_style.body.grad_color = LV_COLOR_RED;//活跃刻度线的终止颜色
//	main_style.line.color = LV_COLOR_SILVER;//非活跃刻度线的颜色
//	main_style.line.width = 2;//每一条刻度线的宽度
//	main_style.body.padding.ver = 16;//每一条刻度线的长度
//
//	//2.创建一个刻度指示器对象
//	lmeter = lv_lmeter_create(scr,NULL);
//	lv_obj_set_size(lmeter,180,180);//设置大小
//	lv_obj_align(lmeter,NULL,LV_ALIGN_CENTER,0,0);//与屏幕保持居中对齐
//	lv_lmeter_set_range(lmeter,0,100);//设置进度范围
//	lv_lmeter_set_value(lmeter,lmeter_value);//设置当前的进度值
//	lv_lmeter_set_scale(lmeter,240,31);//设置角度和刻度线的数量
//	lv_lmeter_set_style(lmeter,&main_style);//设置样式
//
//	//3.创建一个 label 标签来显示当前的进度值
//	label1 = lv_label_create(scr,NULL);
//	lv_obj_align(label1,lmeter,LV_ALIGN_CENTER,0,0);//设置与 lmeter1 居中对齐
//	//使能自动对齐功能,当文本长度发生变化时,它会自动对齐的
//	lv_obj_set_auto_realign(label1,true);
//	lv_label_set_text(label1,"0%");//设置文本
//
//	//4.创建一个任务来模拟 lmeter 的加载
//	lv_task_create(dash_cb,500,LV_TASK_PRIO_MID,NULL);

}
