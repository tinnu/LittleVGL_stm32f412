#ifndef LEMTER_TEST_H
#define LEMTER_TEST_H

#include "lvgl.h"

void dash_cb(lv_task_t *task);
void lv_lemter_add();

void set_gaugeTest(int16_t val);
void lvAdd_gaugeTest();
void cb_gaugeTest(lv_task_t *gauge_test_task);

void lvAdd_sliderTest();

#endif
