#include <lv_examples/terminal/lemter_test.h>
#include "lvgl.h"
#include <stdio.h>

static lv_obj_t *gauge1;
static lv_obj_t * label1;

static int16_t speed_val = 0;
static lv_style_t gauge_style;

void set_gaugeTest(int16_t val)
{
	char buff[40];
	if(speed_val==val)
		return;
	speed_val = val;
	lv_gauge_set_value(gauge1,0,val);
	if(speed_val<60)
		sprintf(buff,"#5FB878 %d km/h#",val);//显示绿色,代表安全
	else if(speed_val<100)
		sprintf(buff,"#FFB800 %d km/h#",val);//显示黄色,代表警告
	else
		sprintf(buff,"#FF0000 %d km/h#",val);//显示红色,代表危险
	lv_label_set_text(label1,buff);
}

void cb_gaugeTest(lv_task_t *gauge_test_task)
{
	static uint8_t is_add_dir = 1;//是否是速度增加的方向
	char buff[40];
	if(is_add_dir)
	{
		speed_val += 5;
		if(speed_val>=200)
			is_add_dir = 0;
	}else
	{
		speed_val -= 5;
		if(speed_val<=0)
			is_add_dir = 1;
	}
	//设置指针的数值
	lv_gauge_set_value(gauge1,0,speed_val);
	//把此速度显示在标签上,然后根据不同大小的数值显示出不同的文本颜色
	if(speed_val<60)
		sprintf(buff,"#5FB878 %d km/h#",speed_val);//显示绿色,代表安全
	else if(speed_val<100)
		sprintf(buff,"#FFB800 %d km/h#",speed_val);//显示黄色,代表警告
	else
		sprintf(buff,"#FF0000 %d km/h#",speed_val);//显示红色,代表危险
		lv_label_set_text(label1,buff);
}

void lvAdd_gaugeTest()
{
//	lv_obj_t * scr = lv_scr_act();//获取当前活跃的屏幕对象
//	lv_color_t needle_colors[1];
//
//	//1-配置结构体lv_style_t
//	lv_style_copy(&gauge_style, &lv_style_pretty_color);
//	//关键数值点之前的刻度线的起始颜色,为浅绿色
//	gauge_style.body.main_color = LV_COLOR_MAKE(0x5F,0xB8,0x78);
//	//关键数值点之前的刻度线的终止颜色,为浅黄色
//	gauge_style.body.grad_color = LV_COLOR_MAKE(0xFF,0xB8,0x00);
//	gauge_style.body.padding.ver = 16;	//每条刻度长度
//	gauge_style.body.padding.inner = 3;	//标签与刻度距离
//	//中心圆点的颜色
//	gauge_style.body.border.color = LV_COLOR_MAKE(0x33,0x33,0x33);
//	gauge_style.line.width = 2;	//线宽
//	gauge_style.text.color = LV_COLOR_BLACK;	//文本颜色
//	gauge_style.line.color = LV_COLOR_SILVER;	//刻度线颜色
//
//	//2-创建仪表盘
//	gauge1 = lv_gauge_create(scr, NULL);
//	lv_obj_set_size(gauge1,200,200);//设置仪表盘的大小
//	lv_gauge_set_style(gauge1,&gauge_style);//设置样式
//	lv_gauge_set_range(gauge1,0,200);//设置仪表盘的范围
//	needle_colors[0] = LV_COLOR_BLUE;
////	needle_colors[1] = LV_COLOR_PURPLE;
//	//设置指针的数量和其颜色
//	lv_gauge_set_needle_count(gauge1,sizeof(needle_colors)/sizeof(needle_colors[0]),needle_colors);
//	//设置指针 1 指向的数值,我们把指针 1 当作速度指针吧
//	lv_gauge_set_value(gauge1,0,speed_val);
////	lv_gauge_set_value(gauge1,1,90);//设置指针二指向的数值,就让它指向关键数值点吧
//	lv_gauge_set_critical_value(gauge1,100);//设置关键数值点
//	lv_gauge_set_scale(gauge1,200,21,5);//设置角度,刻度线的数量,数值标签的数量
//	lv_obj_align(gauge1,NULL,LV_ALIGN_CENTER,0,0);//设置与屏幕居中对齐
//
//	//3.创建一个标签来显示指针 1 的数值
//	label1 = lv_label_create(scr,NULL);
//	lv_label_set_long_mode(label1,LV_LABEL_LONG_BREAK);//设置长文本模式
//	lv_obj_set_width(label1,80);//设置固定的宽度
//	lv_label_set_align(label1,LV_LABEL_ALIGN_CENTER);//设置文本居中对齐
//	lv_label_set_style(label1,&lv_style_pretty);//设置样式
//	lv_label_set_body_draw(label1,true);//使能背景重绘制
//	lv_obj_align(label1,gauge1,LV_ALIGN_CENTER,0,60);//设置与 gauge1 的对齐方式
//	lv_label_set_text(label1,"0 km/h");//设置文本
//	lv_label_set_recolor(label1,true);//使能文本重绘色

	//4.创建一个任务来模拟速度指针的变化
//	lv_task_create(cb_gaugeTest,1000,LV_TASK_PRIO_MID,NULL);
}
