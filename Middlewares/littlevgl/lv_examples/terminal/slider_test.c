/*
 * slider_test.c
 *
 *  Created on: 2020年7月25日
 *      Author: AN
 */
#include <lv_examples/terminal/lemter_test.h>
#include "lvgl.h"
#include <stdio.h>

static lv_obj_t * slider_label;

static void cb_sliderTest(lv_obj_t * slider, lv_event_t event)
{
    if(event == LV_EVENT_VALUE_CHANGED) {
        static char buf[4]; /* max 3 bytes for number plus 1 null terminating byte */
        snprintf(buf, 4, "%u", lv_slider_get_value(slider));
        lv_label_set_text(slider_label, buf);
    }
}


//例程入口
void lvAdd_sliderTest()
{
    lv_obj_t * slider = lv_slider_create(lv_scr_act(), NULL);
    lv_obj_set_width(slider, LV_DPI*1.5);
    lv_obj_align(slider, NULL, LV_ALIGN_CENTER, 0, 0);
    lv_obj_set_event_cb(slider, cb_sliderTest);
    lv_slider_set_range(slider, 0, 100);
    // Create a label below the slider
    slider_label = lv_label_create(lv_scr_act(), NULL);
    lv_label_set_text(slider_label, "0");
    lv_obj_set_auto_realign(slider_label, true);
    lv_obj_align(slider_label, slider, LV_ALIGN_OUT_BOTTOM_MID, 0, 0);
}
